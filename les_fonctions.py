import csv
import datetime
import random
import listes_hasard as ldh



# On importe les données du fichier csv
def importer_data(fichier):
    '''
    Importe les données à partir d'un fichier CSV et retourne une liste à deux dimensions
     contenant les données du fichier, en excluant la première ligne.

     Args:
         fichier (str): Le chemin vers le fichier CSV.

     Returns:
         list: Une liste à deux dimensions contenant les données du fichier.
    '''
    tab_data=[]
    #on ouvre le fichier original
    with open(fichier, "r+") as data_marees :
        marees= csv.reader(data_marees)
        #on injecte les données du fichier dans une variable
        for row in marees :
           tab_data.append(row)
        #on ferme le fichier original
        data_marees.close()
        del tab_data[0]
        return tab_data

# On stocke les données dans une constante
TAB_DATA_MAREES=importer_data("marees_v1.csv")

# On déclare les index du tableau de données en constantes
NO_CAPTEUR=0
CORD_X=1
CORD_Y=2
DATE_HAEURE=3
ACTIF=4
### 1- FONCTIONS POUR LA MANIPULATION DES DONNEES
# On récupère les dates de marées contenues dans le fichier
def liste_dates_heures(liste):
    '''
        Retourne une liste des dates et heures des marées enregistrées à partir d'un tableau de données.

        Args:
            liste (list): Le tableau de données.

        Returns:
            list: Une liste de dates et heures des marées enregistrées.
    '''
    les_dates_heures=[]
    for el in liste:
        # On parcours le tableau est on y extrait les dates sans répétition
        if el[DATE_HAEURE] not in les_dates_heures :
            les_dates_heures.append(el[DATE_HAEURE])
    return les_dates_heures

# On crée une matrice nulle à taille de 3 lignes et 3 colonnes
def nouvelle_matrice_nulle():
    '''
       Retourne une matrice nulle de 3 lignes et 3 colonnes.

       Returns:
           list: Une matrice nulle de 3 lignes et 3 colonnes.
    '''
    matrice_nulle = []
    for e in range(3):
        une_ligne = [0] * (3)
        matrice_nulle = matrice_nulle + [une_ligne]
    return matrice_nulle
#On crée un dictionnaire de matrices d'activité des capteurs pour schématiser et faciliter la manipulation
def implementer_dic_matrices(liste):
    '''
        Crée un dictionnaire de matrices d'activité des capteurs pour chaque date et heure de marée.

        Args:
            liste (list): Le tableau de données.

        Returns:
            dict: Un dictionnaire avec les dates et heures de marée comme clés et des matrices comme valeurs.
    '''
    def creer_dictionnaire(liste):
        '''Cette fonction prend en paramètre le tableau de données et retourne un dictionnaire
         avec la date et l'heure de la marées comme clé et une matrice nulle comme valeur
         '''
        dic = {}
        # on crée et parcours une liste des dates afin de mettre la date en clé et lui attribuer la matrice en valeur
        for e in liste_dates_heures(liste):
            dic[e] = nouvelle_matrice_nulle()
        return dic
    #on crée une variable locale contenat notre dictionnaire
    dic=creer_dictionnaire(liste)
    #on parcours notre dictionnaire vide et on lui ajoute les nouvelles valeurs de tableau
    row=0
    for element in dic:
        for ligne in range(len(dic[element])):
            for colonne in range(len(dic[element][ligne])):
                dic[liste[row][DATE_HAEURE]][ligne][colonne]=int(liste[row][ACTIF])
                row+=1
    return dic
# On implémente et stock le dictionnaire dans une constante
DIC_MATRICES=implementer_dic_matrices(TAB_DATA_MAREES)

# On crée une matrice qui représente l'activité finale
def matrice_finale(dic):
    '''
        Extrait et retourne une matrice représentant l'activité globale de chaque capteur à partir d'un dictionnaire de matrices.

        Args:
            dic (dict): Le dictionnaire de matrices.

        Returns:
            list: Une matrice représentant l'activité globale de chaque capteur.
        '''
    matrice=nouvelle_matrice_nulle()
    #on parcours le dictionnaire de matrice et on additionne les valeurs positifs
    for element in dic:
        for ligne in range(3):
            for colonne in range(3):
                matrice[ligne][colonne]+=dic[element][ligne][colonne]
    return matrice

### 2- FONCTIONS POUR REPONDRE AUX BESOINS DU PROGRAMME
# fonction pour le plus haut point atteint
def plus_haut_point(liste):
    ''' Cette fonction prend en paramètre le tableau de données et retourne une liste de coordonnées
    du plus haut point atteint
    '''
    dic=implementer_dic_matrices(liste)
    point=[0,0]
    for element in dic:
        for ligne in range(3):
            for colonne in range(3):
                if dic[element][ligne][colonne]==1 :
                    point[0]=ligne
                    point[1]=colonne
    return point

# fonction pour les points tjrs immergés
def points_tjrs_immerges(liste):
    '''
        Cette fonction prend en paramètre le tableau de données et retourne un dictionnaire des points
        qui se retrouvent toujours immergés.

        Args:
            liste (list): Le tableau de données.

        Returns:
            dict: Un dictionnaire contenant les points toujours immergés avec leurs coordonnées.
       '''
    matrice_d_activite=matrice_finale(liste)
    les_point_immerges={}
    no_capteur=0
    for x in range(len(matrice_d_activite)) :
        for y in range(len(matrice_d_activite)):
            no_capteur+=1
            if matrice_d_activite[x][y] == 5 :
                les_point_immerges[str(no_capteur)]=[x,y]
    return les_point_immerges

# fonction pour les points jamais immergés
def points_jamais_immerges(liste):
    '''
       Cette fonction prend en paramètre le tableau de données et retourne un dictionnaire des points
       qui ne se retrouvent jamais immergés.

       Args:
           liste (list): Le tableau de données.

       Returns:
           dict: Un dictionnaire contenant les points jamais immergés avec leurs coordonnées.
       '''
    matrice_d_activite=matrice_finale(liste)
    les_point_immerges={}
    no_capteur=0
    for x in range(len(matrice_d_activite)) :
        for y in range(len(matrice_d_activite)):
            no_capteur+=1
            if matrice_d_activite[x][y] == 0 :
                les_point_immerges[str(no_capteur)]=[x,y]
    return les_point_immerges

# fonction pour le coefecient maximum
def coef_max(liste):
    '''
       Cette fonction prend en paramètre le tableau de données et retourne en chaîne de caractères
       la date du jour où la marée a atteint le plus grand coefficient d'activité.

       Args:
           liste (list): Le tableau de données.

       Returns:
           str: La date du jour avec le plus grand coefficient d'activité.
       '''
    dict_date=implementer_dic_matrices(liste)
    date=""
    v_max=max(dict_date.values())
    for cle in dict_date :
        if dict_date[cle]==v_max :
            date = cle
    return date

#fonction pour le coefecient minimum
def coef_min(liste):
    '''
        Cette fonction prend en paramètre le tableau de données et retourne en chaîne de caractères
        la date du jour où la marée a atteint le plus petit coefficient d'activité.

        Args:
            liste (list): Le tableau de données.

        Returns:
            str: La date du jour avec le plus petit coefficient d'activité.
       '''
    dict_date=implementer_dic_matrices(liste)
    date=""
    v_min=min(dict_date.values())
    for cle in dict_date:
        if dict_date[cle]==v_min :
            date = cle
    return date

### 3- FONCTIONS POUR LA MANIPULATION DE DATE-TIME
def date_heure_activite_capteurs(un_dic):
    '''
        Cette fonction prend en paramètre un dictionnaire de données et détermine les dates et heures
        d'activité ou de non-activité pour chaque capteur.

        Args:
            un_dic (dict): Un dictionnaire de données.

        Returns:
            dict: Un dictionnaire contenant les activités de chaque capteur avec des dates et heures.
       '''
    points_actifs= {"1":[],"2":[],"3":[],"4":[],"5":[],"6":[],"7":[],"8":[],"9":[],}
    matrice=nouvelle_matrice_nulle()
    coordonees=[]
    for x in range(len(matrice)):
        for y in range(len(matrice)):
            coordonees.append([x,y])
    capteur=0
    for coord in coordonees:
        capteur+=1
        for element in un_dic:
            if un_dic[element][coord[0]][coord[0]]:
                points_actifs[str(capteur)].append([element,"actif"])
            else :
                points_actifs[str(capteur)].append([element,"inactif" ])
    return points_actifs

# fonction pour détermine les dates de début et de fin d'un capteur donné
def periodes_activite_capteur(un_dic,no_capteur):
    '''
        Cette fonction prend en paramètre un dictionnaire de données et un numéro de capteur,
        puis détermine les dates de début et de fin d'activité pour le capteur spécifié.

        Args:
            un_dic (dict): Un dictionnaire de données.
            no_capteur (int): Le numéro du capteur à analyser.

        Returns:
            list: Une liste contenant des dictionnaires avec les dates de début et de fin d'activité.
       '''
    capteurs=[]
    activites=date_heure_activite_capteurs(un_dic)
    actif=False
    for el in activites[no_capteur]:
        if el[1]== "actif" and actif==False :
            capteurs.append({"debut":el[0]})
            actif=True
        elif el[1]== "inactif" and actif==True :
            capteurs.append({"fin":el[0]})
    return capteurs

#fonction pour mettre en forme les dates
def formater_date(da):
    '''
        Cette fonction prend en paramètre une date au format chaîne de caractères et la formate en
        un objet datetime.

        Args:
            da (str): Une date au format chaîne de caractères (ex: "YYYY-MM-DD-HH").

        Returns:
            datetime.datetime: Un objet datetime formatté.
       '''
    d=da.split('-')
    new_da=[]
    for e in d:
        new_da.append(int(e.lstrip("0")))
    return datetime.datetime(new_da[0], new_da[1], new_da[2], new_da[3])
def duree(d1,d2):
    '''
        Cette fonction calcule la durée entre deux dates datetime.

        Args:
            d1 (datetime.datetime): La première date.
            d2 (datetime.datetime): La deuxième date.

        Returns:
            datetime.timedelta: La durée entre les deux dates.
       '''
    return d2-d1

#fonction pour la question 4 pourcentage

# fonction pour calculer le pourcentage
def calcul_pourcentage():
    '''
        Cette fonction calcule le pourcentage de temps pendant lequel un capteur s'est retrouvé sous l'eau.

        Returns:
            float: Le pourcentage de temps d'immersion du capteur.
       '''
    while True:
        dd_user=random.choice(ldh.LISTE_DATES_HASARD)
        print("L'utilisateur a choisi la date de début : ", dd_user)
        df_user=random.choice(ldh.LISTE_DATES_HASARD)
        print("L'utilisateur a choisi la date de fin : ", df_user)
        no_capteur=random.choice(ldh.LISTE_CAPTEURS_HASARD)
        print("L'utilisateur a choisi le capteur numéro : ", no_capteur)
        if dd_user < df_user :
            break
    pa_capteur=periodes_activite_capteur(DIC_MATRICES,no_capteur)
    dd_capteur=formater_date(pa_capteur[0]["debut"])
    df_capteur=formater_date(pa_capteur[1]["fin"])
    new_dd_user=formater_date(dd_user)
    new_df_user=formater_date(df_user)
    if new_dd_user <= dd_capteur and new_df_user<=dd_capteur or new_dd_user >= df_capteur and new_df_user>=df_capteur:
        return 0
    elif new_dd_user >= dd_capteur and new_df_user<=df_capteur :
        return 100
    elif new_dd_user <= dd_capteur and new_df_user>=df_capteur :
        return (duree(dd_capteur,df_capteur)/duree(dd_user,df_user))*100
    elif new_dd_user <= dd_capteur and new_df_user<=df_capteur :
        return (duree(dd_capteur,df_user)/duree(dd_user,df_user))*100
    elif new_dd_user >= dd_capteur and new_df_user>=df_capteur :
        return (duree(dd_user,df_capteur)/duree(dd_user,df_user))*100
    else :
        raise Exception("Désolé ! Les donées saisies ne permettent pas le calcul de pourcentage !")

# MENU AFFICHER AU LANCEMENT DU PROGRAMME "SIMULATION OCEANOGRAPHIQUE"
def menu():
    '''
      Cette fonction affiche un menu de navigation au lancement du programme "SIMULATION OCEANOGRAPHIQUE".
      Le menu présente différentes options disponibles pour l'utilisateur.

      Options disponibles :
      1. Afficher la position (x, y) sur la plage où la marée a atteint le plus haut point.
      2. Afficher l'ensemble des capteurs toujours immergés.
      3. Afficher l'ensemble des capteurs jamais immergés.
      4. Calculer le pourcentage de temps pendant lequel un capteur s'est retrouvé sous l'eau.
      5. Afficher la date du jour où le coefficient de marée était le plus élevé.
      6. Afficher la date du jour où le coefficient de marée était le plus bas.
      '''
    print(50 * "=")
    print("🌍 SIMULATION OCEANOGRAPHIQUE                v1.0")
    print(50 * "-")
    print("Melki | Fabien | Romain                   CDA 2023")
    print(50 * "-")
    print(" " * 15, " Hello World 🖐")
    print("1. position (x,y) sur la plage,\n"
          "   où la marée a atteint le plus haut point")
    print("2. Ensemble des capteurs toujours immergés")
    print("3. Ensemble des capteurs jamais immergés")
    print("4. Pourcentage de temps pendant lequel\n"
          "   un capteur s'est retrouvé sous l'eau.")
    print("5. Plus haut coefficient")
    print("6. Plus bas coefficient\n")

    while True:
        user_input = input("Entrez une option : ")

        # Vérifie si l'entrée est un nombre et non un string.
        if user_input.isdigit():
            menu_choice = int(user_input)

            if menu_choice == 1:
                print("la position (x,y) sur la plage, où la marée a atteint le plus haut point :\n",
                      "X = ", plus_haut_point(TAB_DATA_MAREES)[0],
                      " - Y = ", plus_haut_point(TAB_DATA_MAREES)[1])
                menu()
            elif menu_choice == 2:
                print("L'ensemble des points qui se retrouvent toujours immergés :")
                for element in points_tjrs_immerges(DIC_MATRICES):
                    print("Le point représenté par le capteur ", element," qui se trouve à la position : \n",
                          " X = ", points_tjrs_immerges(DIC_MATRICES)[element][0]," -  Y = ", points_tjrs_immerges(DIC_MATRICES)[element][1])
                menu()
                break
            elif menu_choice == 3:
                print("L'ensemble des points qui ne se retrouvent jamais immergés :")
                for element in points_jamais_immerges(DIC_MATRICES):
                    print("Le point représenté par le capteur ", element, " qui se trouve à la position : \n",
                          " X = ", points_jamais_immerges(DIC_MATRICES)[element][0], " -  Y = ",
                          points_jamais_immerges(DIC_MATRICES)[element][1])
                menu()
                break
            elif menu_choice == 4:
                print("Le pourcentage de temps pendant lequel ce point s'est retrouvé sous l'eau : ", calcul_pourcentage(),"%")
                menu()
                break
            elif menu_choice == 5:
                print(" La date du jour où il y a eu le plus gros coefficient de marée est :")
                print(" "*2, coef_max(TAB_DATA_MAREES))

                menu()
                break
            elif menu_choice == 6:
                print(" La date du jour où il y a eu le plus faible coefficient de marée est :")
                print(" "*2, coef_min(TAB_DATA_MAREES))
                menu()
                break
            else:
                print("Merci de saisir une option valide. 1, 2, 3, 4, 5 ou 6")
        else:
            print("Merci de saisir une option valide. 1, 2, 3, 4, 5 ou 6")
